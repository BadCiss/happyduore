import { Component, OnInit, VERSION } from "@angular/core";

@Component({
  selector: "app-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.css", "../../../assets/responsive.css"]
})
export class NavbarComponent implements OnInit {
  name: String = "JessObvious";
  version: String = `Ng v.${VERSION.full}`;
  constructor() {}

  ngOnInit() {}
}
