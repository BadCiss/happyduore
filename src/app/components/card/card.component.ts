import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
//import {ListUsersService } from './../../services/list-users.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],

})
export class CardComponent implements OnInit {

  @Input() data : JSON;
  @Input() order : boolean;

  constructor() { }

    ngOnInit() {
        console.log(this.data);
        console.log(this.order);
    }

}
