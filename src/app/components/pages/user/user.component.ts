import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, RouterLink } from "@angular/router";
import { ServerService } from "../../../services/server.service";
import { Observable } from "rxjs/Observable";
import { User } from "../../../main/models/user";
import { Children } from "../../../main/models/children";
import { environment } from "../../../../environments/environment";
import { CoreEnvironment } from "@angular/core/src/render3/jit/compiler_facade_interface";
import { Location } from "@angular/common";

@Component({
  selector: "app-user",
  templateUrl: "./user.component.html",
  styleUrls: ["./user.component.css", "../../../../assets/responsive.css"],
  preserveWhitespaces: true
})
export class UserComponent implements OnInit {
  private readonly profilePic_path = environment.profil_picture;

  //private user$: Observable<User[]>;

  private user$: Observable<any>;
  private user: [];
  private children$: Observable<Children[]>;

  constructor(
    private route: ActivatedRoute,
    private serviceServer: ServerService,
    private _location: Location
  ) {}

  backClicked() {
    this._location.back();
  }

  ngOnInit() {
    let id = this.route.snapshot.params["id"];
    //this.user$ = this.serviceServer.user_list(id);

    // this.children$ = this.serviceServer.user_child(id);
    this.user$ = this.route.snapshot.data["userd"];

    // this.route.data.subscribe(data => {
    //   console.log(data["userd"][0]);
    //   return (this.user$ = data["userd"][0]);
    // });
  }
}
