import { Component, Input, OnInit } from "@angular/core";
import { ServerService } from "../../../services/server.service";
import { User } from "../../../main/models/user";
import { Observable } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import fontawesome from "@fortawesome/fontawesome";
import { ActivatedRoute, Router, RouterLink } from "@angular/router";

@Component({
  selector: "app-homepage",
  templateUrl: "./homepage.component.html",
  styleUrls: ["./homepage.component.css"],
  preserveWhitespaces: true
})
export class HomepageComponent implements OnInit {
  start: number = 0;
  nbdisplay: number = 5;

  constructor(
    private serverService: ServerService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  user$: Observable<User[]>;
  page: number = this.route.snapshot.queryParams.page;
  option: string = this.route.snapshot.fragment;
  filter: string;

  nextPage() {
    this.start += this.nbdisplay;
    this.user$ = this.serverService.user_list_limit(this.start, this.nbdisplay);
  }

  previousPage() {
    this.start -= this.nbdisplay;
    this.user$ = this.serverService.user_list_limit(this.start, this.nbdisplay);
    //this.router.navigate(['/?page=2']);
    this.router.navigate(["/"], { queryParams: { page: "2" } });
  }

  setFilter(getfilter: string) {
    if (getfilter === "proximity") {
    }
    if (getfilter === "online") {
    }
    if (getfilter === "new") {
    }
    this.filter = getfilter;
  }

  ngOnInit() {
    //this.user$ = this.serverService.user_list_limit(this.start,this.nbdisplay);
    this.user$ = this.route.snapshot.data["list"];
    console.log(this.page);
    console.log(this.option);
  }
}
