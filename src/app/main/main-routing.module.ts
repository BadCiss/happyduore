import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule, RouterLink } from "@angular/router";

import { UserComponent } from "../components/pages/user/user.component";
import { ContactComponent } from "../components/pages/contact/contact.component";
import { DemandComponent } from "../components/pages/demand/demand.component";
import { InterestComponent } from "../components/pages/interest/interest.component";
import { SettingsComponent } from "../components/pages/settings/settings.component";
import { HomepageComponent } from "../components/pages/homepage/homepage.component";

import { PreloadGuard } from "../guard/preload.guard";
import { ListResolverGuard } from "../guard/list-resolver.guard";

const mainRoutes: Routes = [
  {
    path: "",
    component: HomepageComponent,
    resolve: { list: ListResolverGuard }
  },
  {
    path: ":id",
    component: UserComponent,
    resolve: { userd: PreloadGuard }
  },
  { path: "contact", component: ContactComponent },
  { path: "demand", component: DemandComponent },
  { path: "interest", component: InterestComponent },
  { path: "settings", component: SettingsComponent },
  { path: "**", redirectTo: "", pathMatch: "full" }
];
@NgModule({
  imports: [RouterModule.forChild(mainRoutes)]
})
export class MainRoutingModule {}
