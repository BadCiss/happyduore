export interface User {
    id: number;
    name: string;
    email: string;
    pass: string;
    genre: string;
    birth_date: string;
}
