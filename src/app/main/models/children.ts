export interface Children {
  id_user: number;
  genre: boolean;
  birth_date: Date;
}
