import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CardComponent } from "./../components/card/card.component";
import { HomepageComponent } from "./../components/pages/homepage/homepage.component";

import { InterestComponent } from "./../components/pages/interest/interest.component";
import { DemandComponent } from "./../components/pages/demand/demand.component";
import { SettingsComponent } from "../components/pages/settings/settings.component";
import { ContactComponent } from "../components/pages/contact/contact.component";
import { UserComponent } from "../components/pages/user/user.component";
import { AgePipe } from "./pipes/age";
import { GenrePipe } from "./pipes/genre";
import { PreloadGuard } from "../guard/preload.guard";
import { MainRoutingModule } from "./main-routing.module";
import { RouterModule } from "@angular/router";

@NgModule({
  declarations: [
    CardComponent,
    HomepageComponent,
    InterestComponent,
    DemandComponent,
    SettingsComponent,
    ContactComponent,
    UserComponent,
    AgePipe,
    GenrePipe
  ],
  imports: [CommonModule, RouterModule, MainRoutingModule],
  providers: [PreloadGuard]
})
export class MainModule {}
