import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "genre"
})
export class GenrePipe implements PipeTransform {
  transform(value, translate?): String {
    if (translate) {
      return translate;
    } else {
      if (value == 1) {
        return "boy";
      } else {
        return "girl";
      }
    }
  }
}
