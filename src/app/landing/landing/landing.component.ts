import { Component, OnInit } from "@angular/core";
import {
  MatDialog,
  MatDialogConfig,
  MatDialogRef,
  MAT_DIALOG_DATA
} from "@angular/material";
import { LoginSignupComponent } from "./../login-signup/login-signup.component";
import { TermsComponent } from "./../terms/terms.component";
import { ConfidentialityComponent } from "./../confidentiality/confidentiality.component";
@Component({
  selector: "app-landing",
  templateUrl: "./landing.component.html",
  styleUrls: ["./landing.component.css"]
})
export class LandingComponent implements OnInit {
  constructor(public dialog: MatDialog) {}

  tobeLoaded;

  openDialog(pageTobeloaded: string) {
    if (pageTobeloaded === "login") {
      this.tobeLoaded = LoginSignupComponent;
    } else if (pageTobeloaded === "terms") {
      this.tobeLoaded = TermsComponent;
    } else if (pageTobeloaded === "confidentiality") {
      this.tobeLoaded = ConfidentialityComponent;
    }

    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.closeOnNavigation = true;

    const dialogRef = this.dialog.open(this.tobeLoaded, {
      closeOnNavigation: true
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  ngOnInit() {}
}
