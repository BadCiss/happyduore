import { Component, OnInit } from "@angular/core";
import { ServerService } from "../../services/server.service";
import { MatDialogRef } from "@angular/material";
import { Router } from "@angular/router";
import { LandingComponent } from "../landing/landing.component";

@Component({
  selector: "app-login-signup",
  templateUrl: "./login-signup.component.html",
  styleUrls: ["./login-signup.component.css"]
})
export class LoginSignupComponent implements OnInit {
  registerData = {};
  loginData = {};

  constructor(
    private serverService: ServerService,
    private router: Router,
    private dialogRef: MatDialogRef<LandingComponent>
  ) {}

  logUser() {
    this.serverService.log_user(this.loginData).subscribe(
      res => {
        if (res) {
          console.log(res);
          localStorage.setItem("token", res.token);
          this.router.navigate(["/app/user"]);
        }

        this.dialogRef.close("yeah");
      },
      err => console.log(err)
    );
  }

  registerUser() {
    this.serverService.add_user(this.registerData).subscribe(
      res => {
        console.log(res);
        localStorage.setItem("token", res.token);
        this.router.navigate(["/success"]);
      },
      err => console.log(err)
    );
  }

  ngOnInit() {}
}
