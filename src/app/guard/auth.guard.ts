import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { ServerService } from "../services/server.service";

@Injectable({
  providedIn: "root"
})
export class AuthGuard implements CanActivate {
  constructor(private serverservice: ServerService, private router: Router) {}

  canActivate(): boolean {
    if (this.serverservice.loggedIn()) {
      return true;
    } else {
      this.router.navigate(["/login"]);
      return false;
    }
  }
}
