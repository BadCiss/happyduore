import { TestBed, async, inject } from '@angular/core/testing';

import { ListResolverGuard } from './list-resolver.guard';

describe('ListResolverGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ListResolverGuard]
    });
  });

  it('should ...', inject([ListResolverGuard], (guard: ListResolverGuard) => {
    expect(guard).toBeTruthy();
  }));
});
