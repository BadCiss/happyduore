import { Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Resolve,
  Router
} from "@angular/router";
import { Observable } from "rxjs";
import { ServerService } from "../services/server.service";
import { HttpErrorResponse } from "@angular/common/http";
import { User } from "../main/models/user";
@Injectable({
  providedIn: "root"
})
export class ListResolverGuard implements Resolve<any> {
  constructor(private serverService: ServerService, private router: Router) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> {
    return this.serverService
      .user_list_limit(1, 5)
      .catch(errorResponse => this.handleError(route, errorResponse));
  }

  handleError(route: ActivatedRouteSnapshot, errorResponse: HttpErrorResponse) {
    switch (errorResponse.status) {
      case 404: {
        this.router.navigate(["/not-found"]);
        return Observable.of(null);
      }
      case 401: {
        const returnURL: string =
          "/" + route.url.map(segment => segment.path).join("/");
        this.router.navigate(["/login"], {
          queryParams: { returnURL: returnURL }
        });
        return Observable.of(null);
      }
      case 403: {
        this.router.navigate(["/unauthorized"]);
        return Observable.of(null);
      }
      // case default: {
      //   console.error("eror");
      //   this.router.navigate(['/error']);
      //   return Observable.of(null);
      // }
    }
  }
}
