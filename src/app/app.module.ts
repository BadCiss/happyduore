import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { AppRoutingModule } from "./app-routing.module";
import { ServerService } from "./services/server.service";
import { CommonModule } from "@angular/common";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";

// import { defineLocale } from 'ngx-bootstrap/chronos';
// import { fr } from 'ngx-bootstrap/locale';

import { AppComponent } from "./app.component";

import { ChatComponent } from "./components/pages/chat/chat.component";
import { CreateUserComponent } from "./components/forms/create-user/create-user.component";
import { EditComponent } from "./components/pages/edit/edit.component";

import { AuthGuard } from "./guard/auth.guard";

import { TokenInterceptorService } from "./services/token-interceptor.service";
import { DropdownDirective } from "./directives/dropdown.directive";

import { CoreModule } from "./core/core.module";
import { LandingModule } from "./landing/landing.module";
import { MainModule } from "./main/main.module";
import { MatButtonModule } from "@angular/material/button";

import { MatInputModule } from "@angular/material/input";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatChipsModule } from "@angular/material/chips";
import { MatSelectModule } from "@angular/material/select";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";
import { ShellComponent } from "./components/shell/shell.component";
import { NavbarComponent } from "./components/navbar/navbar.component";

//import { LoginComponent } from "./landing/login/login.component";
import { LoginSignupComponent } from "./landing/login-signup/login-signup.component";
import { TermsComponent } from "./landing/terms/terms.component";
import { ConfidentialityComponent } from "./landing/confidentiality/confidentiality.component";
// defineLocale('fr', fr);
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

@NgModule({
  declarations: [
    AppComponent,
    ChatComponent,
    CreateUserComponent,
    EditComponent,
    NavbarComponent,
    LoginSignupComponent,
    DropdownDirective,

    ShellComponent,
    TermsComponent,
    ConfidentialityComponent
  ],
  entryComponents: [
    LoginSignupComponent,
    ConfidentialityComponent,
    TermsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    CommonModule,
    ReactiveFormsModule,
    CoreModule,
    LandingModule,
    MainModule,
    AppRoutingModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatCheckboxModule,
    MatChipsModule,
    MatFormFieldModule,
    MatSlideToggleModule
  ],
  providers: [
    ServerService,
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
