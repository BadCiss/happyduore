import { NgModule } from "@angular/core";
import {
  Routes,
  RouterModule,
  RouterLink,
  PreloadAllModules
} from "@angular/router";

import { ChatComponent } from "./components/pages/chat/chat.component";
import { ContactComponent } from "./components/pages/contact/contact.component";
import { EditComponent } from "./components/pages/edit/edit.component";
import { LoginSignupComponent } from "./landing/login-signup/login-signup.component";
import { ShellComponent } from "./components/shell/shell.component";
import { MainModule } from "./main/main.module";
import { LandingModule } from "./landing/landing.module";
const appRoutes: Routes = [
  { path: "chat", component: ChatComponent },
  {
    path: "",
    loadChildren: () => LandingModule
  },
  {
    path: "app",
    component: ShellComponent,
    children: [
      {
        path: "user",
        loadChildren: () => MainModule
      }
    ]
  },
  // {
  //   path: "aa",
  //   component: ShellComponent,
  //   children: [
  //     {
  //       path: "landing",
  //       loadChildren: () => LandingModule
  //     }
  //   ]
  // },
  { path: "edit", component: EditComponent },
  { path: "oups", component: ContactComponent },
  { path: "test", component: LoginSignupComponent },
  { path: "", redirectTo: "test", pathMatch: "full" },
  { path: "**", component: ChatComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {
      preloadingStrategy: PreloadAllModules,
      enableTracing: false
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
