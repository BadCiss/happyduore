import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import "rxjs/Rx";
import { Observable } from "rxjs/Observable";
import { environment } from "../../environments/environment";
import { User } from "../main/models/user";
import { Children } from "../main/models/children";
import { delay } from "rxjs/operator/delay";
import { tap } from "rxjs/operators";

@Injectable()
export class ServerService {
  private readonly API = environment.API + "users/";

  httpOptions = {
    headers: new HttpHeaders({
      "Access-Control-Allow-Origin": "true",
      "Content-Type": "application/json",
      crossDomain: "true"
    }),
    withCredentials: false
  };

  constructor(private httpClient: HttpClient) {}

  user_list(id?) {
    let apiRequested = this.API;
    if (id) {
      apiRequested = this.API + id;
    }

    return this.httpClient.get<User[]>(apiRequested, this.httpOptions);
  }

  user_child(id) {
    return this.httpClient.get<Children[]>(
      this.API + id + "/children",
      this.httpOptions
    );
  }

  user_list_limit(start, limit) {
    let apiRequested = this.API + "/limit/" + start + "/" + limit;
    return this.httpClient.get<User[]>(apiRequested, this.httpOptions);
  }

  add_user(data) {
    return this.httpClient.post<any>(this.API, data);
  }

  log_user(data) {
    console.log(data);
    console.log(this.API + "signin");
    return this.httpClient.post<any>(this.API + "signin", data);
  }

  check_name(name) {
    return this.httpClient.post<any>(this.API + "checkname", name);
  }

  loggedIn() {
    return !!localStorage.getItem("token");
  }

  getToken() {
    return localStorage.getItem("token");
  }
}
