import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Router } from "@angular/router";
import { HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class ErrorHandlingService {
  constructor(private router: Router) {}

  handleError(route: ActivatedRouteSnapshot, errorResponse: HttpErrorResponse) {
    switch (errorResponse.status) {
      case 404: {
        this.router.navigate(["/not-found"]);
        return Observable.of(null);
      }
      case 401: {
        const returnURL: string =
          "/" + route.url.map(segment => segment.path).join("/");
        this.router.navigate(["/login"], {
          queryParams: { returnURL: returnURL }
        });
        return Observable.of(null);
      }
      case 403: {
        this.router.navigate(["/unauthorized"]);
        return Observable.of(null);
      }
      // case default: {
      //   console.error("eror");
      //   this.router.navigate(['/error']);
      //   return Observable.of(null);
      // }
    }
  }
}
