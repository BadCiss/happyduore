import { Injectable, Injector } from "@angular/core";
import { HttpInterceptor } from "@angular/common/http";
import { ServerService } from "./server.service";

@Injectable()
export class TokenInterceptorService implements HttpInterceptor {
  constructor(private injector: Injector) {}

  intercept(req, next) {
    let authService = this.injector.get(ServerService);
    let tokenizedReq = req.clone({
      setHeaders: {
        Authorization: `Bearer VV ${authService.getToken()}`
      }
    });
    return next.handle(tokenizedReq);
  }
}
