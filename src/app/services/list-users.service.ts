import { Injectable, OnInit } from '@angular/core';

@Injectable()
export class ListUsersService{
  private list_user = [
  {
    id:1,
    pseudo:"Test1",
    age:22,
    city:"Testville",
    cp:11,
    img1_url:"assets/mockup/avatarhomme.jpg",
    img2_url:"",
    headline:"Decouvre moi"
  },
  {
    id:2,
    pseudo:"Test2",
    age:22,
    city:"Grouville",
    cp:11,
    img1_url:"",
    img2_url:"",
    headline:"headline2"
  },
  {
    id:3,
    pseudo:"Test3",
    age:22,
    city:"bougille",
    cp:32,
    img1_url:"",
    img2_url:"",
    headline:"headline3"
  }];

}
