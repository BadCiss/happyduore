var express = require('express');
var bodyParser = require("body-parser");
var cors = require('cors');
var http = require('http');


var users = require('./express/routes/users');
var config = require('./express/routes/config');

var hostname = 'localhost';
//var port = process.env.PORT || 8888;
var port = 3000;
var app = express();

app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(bodyParser.json());

// Use middleware to set the default Content-Type
app.use(function (req, res, next) {
  res.header('Content-Type', 'application/json');
  next();
});

var urlAuthorized = [
  'http://localhost:4200', //this is my front-end url for development
];

var corsOptions = {
  origin: function (origin, callback) {
    var isWhitelisted = urlAuthorized.indexOf(origin) !== -1;
    callback(null, isWhitelisted);
  },
  credentials: true
}
//here is the magic
app.use(cors(corsOptions));


var myRouter = express.Router();

myRouter.route('/')
  .all(function (req, res) {
    res.json({
      message: "API Works",
      methode: req.method
    });
  });

app.use('/users', users);
app.use('/config', config);
app.get("*", (req, res) => {
  res.sendStatus(404);
});

app.listen(port, hostname, function () {
  console.log("Le serveur fonctionne sur http://" + hostname + ":" + port);
});

module.exports = app;
