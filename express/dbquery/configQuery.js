var db=require('./dbconnection'); //reference of dbconnection.js

var ConfigQuery={

  getAllConfig:function(callback){
    return db.query("Select * from config",callback);
  },
  getConfigById:function(id,callback){
    return db.query("select * from config where Id=?",[id],callback);
  },
  addConfig:function(User,callback){
    return db.query("Insert into config values(?,?,?)",[User.Id,User.Title,User.Status],callback);
  },
  deleteConfig:function(id,callback){
    return db.query("delete from config where Id=?",[id],callback);
  },
  updateConfig:function(id,User,callback){
    return db.query("update user set Title=?,Status=? where Id=?",[User.Title,User.Status,id],callback);
  }

};
module.exports=ConfigQuery;
