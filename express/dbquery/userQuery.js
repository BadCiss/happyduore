var db = require('./dbconnection'); //reference of dbconnection.js

var UserQuery = {

  getAllUsers: function (callback) {
    return db.query("SELECT * FROM user", callback);
  },

  getUserList: function (start, nbresult, callback) {

    return db.query("SELECT * FROM user LIMIT ?,?", [start, nbresult], callback);
  },

  getUserWithCredential: function (data, callback) {
    return db.query("SELECT id FROM user WHERE pass = ? AND  email =  ?", [data.password, data.email, ], callback);
  },

  checkName: function (data, callback) {
    return db.query("SELECT name FROM user WHERE name = ?)", [data.name], callback);
  },

  checkMail: function (data, callback) {
    return db.query("SELECT email FROM user WHERE email = ? )", [data.email], callback);
  },

  getUserListWithFilter: function (filter, start, nbresult, callback) {

    return db.query("SELECT * FROM user " + filter + " LIMIT  ?,?", [start, nbresult], callback);
  },

  getUserById: function (id, callback) {
    return db.query("SELECT * FROM user where Id=?", [id], callback);
  },

  getUserById: function (id, callback) {
    return db.query("SELECT * FROM user WHERE Id=? ", [id], callback);
  },

  getChildrenByIdUser: function (id, callback) {
    return db.query("SELECT * FROM children WHERE id_user= ?", [id], callback);
  },

  addUser: function (data, callback) {
    //return db.query("INSERT INTO user (email, pass) VALUES (?,?)",[data.email, data.pass],callback);
    return db.query("INSERT INTO user (name, email, pass) VALUES (?,?,?)", [data.name, data.email, data.pass], callback);
  },

  deleteUser: function (id, callback) {
    return db.query("DELETE from user where Id=?", [id], callback);
  },

  updateUser: function (id, user, callback) {
    return db.query("UPDATE user SET Title=?,Status=? where Id=?", [User.Title, User.Status, id], callback);
  }

};
module.exports = UserQuery;
