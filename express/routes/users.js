var express = require("express");
var router = express.Router();
var User = require("../dbquery/userQuery");
var jwt = require("jsonwebtoken");

const key = "repl@ceTh!sKey";

function verifyToken(req, res, next) {
  const genericError = "Unauthorized request";

  if (!req.headers.authorization) {
    return res.status(401).send(genericError);
  }
  let token = req.headers.authorization.split(" ")[1];
  if (token === "null") {
    return res.status(401).send(genericError);
  }
  let payload = jwt.verify(token, key);
  if (!payload) {
    return res.status(401).send(genericError);
  }
  req.userId = payload.subject;
  next();
}

// Return data on specific id or else all id
router.get("/:id?", function (req, res, next) {
  res.setHeader("Content-Type", "application/json");
  if (req.params.id) {
    User.getUserById(req.params.id, function (err, rows) {
      if (err) {
        res.json(err);
      } else {
        res.json(rows);
      }
    });
  } else {
    User.getAllUsers(function (err, rows) {
      if (err) {
        res.json(err);
      } else {
        res.json(rows);
      }
    });
  }
});

// Return data children on specific id
router.get("/:id/children", function (req, res, next) {
  res.setHeader("Content-Type", "application/json");
  if (req.params.id) {
    User.getChildrenByIdUser(req.params.id, function (err, rows) {
      if (err) {
        res.json(err);
      } else {
        res.json(rows);
      }
    });
  }
});

// Return data with specific parameters
router.get("/limit/:starts/:nbresult/:filter?/:match?", function (
  req,
  res,
  next
) {
  res.setHeader("Content-Type", "application/json");
  if (
    req.params.starts &&
    req.params.nbresult &&
    !req.params.filter &&
    !req.params.match
  ) {
    User.getUserList(+req.params.starts, +req.params.nbresult, function (
      err,
      rows
    ) {
      if (err) {
        res.json(err);
      } else {
        res.json(rows);
      }
    });
  } else if (
    req.params.starts &&
    req.params.nbresult &&
    req.params.filter &&
    !req.params.match
  ) {
    console.log(req.params.filter);
    User.getUserListWithFilter(
      req.params.filter,
      +req.params.starts,
      +req.params.nbresult,
      function (err, rows) {
        if (err) {
          res.json(err);
        } else {
          res.json(rows);
        }
      }
    );
  }
});

// Return JWT Token as Sign In if username and password MATCH
router.post("/signin", function (req, res, next) {
  res.setHeader("Content-Type", "application/json");

  User.getUserWithCredential(req.body, function (err, user) {
    if (err || user == 0) {
      res.json(err);
      console.log(err);
    } else {
      let payload = {
        subject: user.id
      };
      let token = jwt.sign(payload, key);
      res.status(200).send({
        token,
        user
      });
      console.log("getUserWithCredential :: " + JSON.stringify(user));
    }
  });
});

// Check if Name is not already taken
router.post("/checkName", function (req, res, next) {
  res.setHeader("Content-Type", "application/json");

  User.checkName(req.body, function (err, user) {
    if (err) {
      res.json(err);
      console.log(err);
    } else {
      console.log(req.body);
      console.log(JSON.stringify(user));
      res.json(req.body); //or return count for 1 &amp;amp;amp; 0
    }
  });
});

// Check if Mail is not already taken
router.post("/checkMail", function (req, res, next) {
  res.setHeader("Content-Type", "application/json");

  User.checkName(req.body, function (err, user) {
    if (err) {
      res.json(err);
      console.log(err);
    } else {
      console.log(req.body);
      console.log(JSON.stringify(user));
      res.json(req.body); //or return count for 1 &amp;amp;amp; 0
    }
  });
});

// Create data for new user
router.post("/", function (req, res, next) {
  res.setHeader("Content-Type", "application/json");

  //if(!req.body.name){ req.body.name= "No name provided"; }
  User.addUser(req.body, function (err, user) {
    if (err) {
      res.json(err);
      console.log(err);
    } else {
      let payload = {
        subject: user.name
      };
      let token = jwt.sign(payload, "repl@ceTh!sKey");
      //res.json(req.body);//or return count for 1 &amp;amp;amp; 0
      res.status(200).send({
        token
      });
      console.log(user);
    }
  });
});

// Delete data on this specific id
router.delete("/:id", function (req, res, next) {
  res.setHeader("Content-Type", "application/json");
  User.deleteUser(req.params.id, function (err, count) {
    if (err) {
      res.json(err);
    } else {
      res.json(count);
    }
  });
});
// Update data on this specific id
router.put("/:id", function (req, res, next) {
  res.setHeader("Content-Type", "application/json");
  User.updateUser(req.params.id, req.body, function (err, rows) {
    if (err) {
      res.json(err);
    } else {
      res.json(rows);
    }
  });
});
module.exports = router;
